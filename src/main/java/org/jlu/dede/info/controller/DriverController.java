package org.jlu.dede.info.controller;
import org.jlu.dede.info.service.InformationService;
import org.jlu.dede.publicUtlis.model.*;
//import org.jlu.dede.model.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
//import org.jlu.dede.feign.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/driver")
public class DriverController {

    //--------------------------------注册
    @Autowired
    InformationService informationService;

    //输入邮箱点击确定，发送验证码
    @RequestMapping(value = "/register/send", method = RequestMethod.POST)
    @ResponseBody
    public boolean registerSend(String emailAccount)
    {
        boolean res = informationService.checkEmailDriver(emailAccount);//检验是否已经存在且唯一
        if (res) {
            boolean inf=false;
            inf=informationService.createVerifyCode(emailAccount);//生成验证码并发送与保存redis
            return inf;
        }
        else
            return false;
    }
    //输入验证码和密码点击确定
    @RequestMapping(value = "/register/verify", method = RequestMethod.POST)
    @ResponseBody
    public boolean registerVerify(String verifyCode, String password,String emailAccount)
    {
        boolean res = informationService.checkVerifyCode(verifyCode,emailAccount);//检验验证码
        if (res) {
            informationService.storePasswordDriver(password, emailAccount);//存储
            return true;
        }
        return false;
    }

    //--------------------------------登录登出
    @RequestMapping(value = "/logIn",method = RequestMethod.POST)
    public ResponseEntity<String> logIn(String emailAccount, String password){
        String token=informationService.checkPasswordDriver(emailAccount,password);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }
    @RequestMapping(value = "/logOut",method = RequestMethod.POST)
    public ResponseEntity<String> logOut(String token) {
        System.out.println(token);
        informationService.logout(token);

        return new ResponseEntity<String>("logout success", HttpStatus.OK);
    }
    @RequestMapping(value = "/verification",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> verification(String token) {
        System.out.println(token);
        Map<String, Object> result= informationService.verification(token);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }


    //--------------------------------个人资料

    //修改完后点击保存(编辑个人资料)
    @RequestMapping(value = "/editUserData/{id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean editUserData(@PathVariable Integer id,@RequestBody Driver driver){
        if(informationService.storeDriver(id,driver))
            return true;
        return false;

    }


    //点击个人信息按钮(查询个人资料)
    @RequestMapping(value = "/queryUserData", method = RequestMethod.GET)
    @ResponseBody
    public Driver queryUserData(Integer id){

        return informationService.returnDriver(id);
    }
    //查询个人账户
    @RequestMapping(value = "/queryDriverAccount", method = RequestMethod.GET)
    @ResponseBody
    public Account queryDriverAccount(Integer id){

        return informationService.queryDriverAccount(id);
    }


    //查询车
    @RequestMapping(value = "/queryCarData", method = RequestMethod.GET)
    @ResponseBody
    public List queryCarData(Integer id){
        return informationService.returnCar(id);
    }


    //增加车
    @RequestMapping(value = "/addCarData/{id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean addCarData(@PathVariable Integer id,@RequestBody Car car){
        if(informationService.storeCar(id,car))
            return true;
        return false;
    }


    //删除车
    @RequestMapping(value = "/deleteCarData", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteCarData(Integer id){
        if(informationService.deleteCar(id))
            return true;
        return false;
    }
    //选择车
    @RequestMapping(value = "/chooseCarData/{id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean chooseCarData(@PathVariable Integer id,@RequestBody Car car){
        if(informationService.chooseCar(id,car))
            return true;
        return false;
    }

    //司机开始快车或者专车接单工作(0:没有开始工作，1:快车，2:专车)
    @RequestMapping(value = "startWork", method = RequestMethod.GET)
    @ResponseBody
    public boolean startWork(Integer id, String state){
          if(informationService.startWork(id,state))
              return true;
          return false;
    }
}
