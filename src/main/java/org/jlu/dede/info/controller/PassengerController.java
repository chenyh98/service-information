package org.jlu.dede.info.controller;
import org.jlu.dede.info.feign.AccountFeign;
import org.jlu.dede.info.service.InformationService;
import org.jlu.dede.publicUtlis.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.jlu.dede.feign.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/passenger")
public class PassengerController {
    //-------------------------------注册
    @Autowired
    InformationService informationService;

    //注册：输入邮箱点击确定(发送验证码)
    @RequestMapping(value = "/register/send", method = RequestMethod.POST)
    @ResponseBody
    public boolean registerSend(String emailAccount)
    {
        boolean res = informationService.checkEmailPassenger(emailAccount);//检验是否已经存在且唯一
        if (res) {
            boolean inf=false;
            inf=informationService.createVerifyCode(emailAccount);//生成验证码并发送与保存redis
            return inf;
        }
        else
            return false;
    }

    //注册：输入验证码和密码点击确定(验证验证码并输入密码)
    @RequestMapping(value = "register/verify", method = RequestMethod.POST)
    @ResponseBody
    public boolean registerVerify(String verifyCode, String password,String emailAccount)
    {
        boolean res = informationService.checkVerifyCode(verifyCode,emailAccount);//检验验证码
        if (res) {
            informationService.storePasswordPassenger(password, emailAccount);//存储
            return true;//+password;
        }
        return false;
    }



    //-----------------------------------登录
    @Autowired
    AccountFeign accountFeign;
    @RequestMapping(value = "/logIn",method = RequestMethod.POST)
    public ResponseEntity<String> logIn(String emailAccount,String password){
        String token=informationService.checkPasswordPassenger(emailAccount,password);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }
    @RequestMapping(value = "/logOut",method = RequestMethod.POST)
    public ResponseEntity<String> logOut(String token) {
        informationService.logout(token);

        return new ResponseEntity<String>("logout success", HttpStatus.OK);
    }

    @RequestMapping(value = "/verification",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> verification(String token) {
        Map<String, Object> result= informationService.verification(token);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    //------------------------------------个人资料

    //
    //修改完后点击保存(编辑个人资料)
    @RequestMapping(value = "/editUserData/{id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean editUserData(@PathVariable Integer id,@RequestBody Passenger passenger){
        if(informationService.storePassenger(id,passenger))
            return true;
        return false;
    }

    //点击个人信息按钮(查询个人资料)
    @RequestMapping(value = "/queryUserData", method = RequestMethod.GET)
    @ResponseBody
    public Passenger queryUserData(Integer id){

        return informationService.returnPassenger(id);
    }
    //查询个人账户
    @RequestMapping(value = "/queryPassengerAccount", method = RequestMethod.GET)
    @ResponseBody
    public Account queryPassengerAccount(Integer id){

        return informationService.queryPassengerAccount(id);
    }

    //点击黑名单按钮(查询黑名单)
    @RequestMapping(value = "/queryBlacklist", method = RequestMethod.GET)
    @ResponseBody
    public List queryBlackList(Integer id){
        return informationService.returnBlackList(id);
    }

    //在历史订单中拉黑司机(增加黑名单)
    @RequestMapping(value = "/addBlacklist/{id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean addBlackList(@PathVariable Integer id, @RequestBody Driver driver){
        if(informationService.storeBlackList(id,driver))
            return true;
        return false;
    }

    //在查看黑名单时删除(删除黑名单)
    @RequestMapping(value = "/deleteBlacklist/{id1}/{id2}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteBlackList(@PathVariable Integer id1,@PathVariable Integer id2){

        if(informationService.deleteBlackListByPerson(id1,id2))
            return true;
        return false;

    }
}
