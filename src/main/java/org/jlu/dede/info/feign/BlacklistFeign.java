package org.jlu.dede.info.feign;


import org.jlu.dede.publicUtlis.model.BlackList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface BlacklistFeign {
    @RequestMapping(value = "/blacklists/{id}",method = RequestMethod.GET)
    //dataaccess中查询黑名单的接口
    public BlackList getByID(@PathVariable Integer id);

    @RequestMapping(value = "/blacklists",method = RequestMethod.POST)
    //dataaccess中增加黑名单的接口
    public void addBlackList(@RequestBody BlackList blackList);

    @RequestMapping(value = "/blacklists/{id}",method = RequestMethod.DELETE)
    //dataaccess中删除黑名单的接口
    public void deleteBlackList(@PathVariable Integer id);

    @RequestMapping(value = "/blacklists/{id}",method = RequestMethod.POST)
    public void updateBlackList(@PathVariable Integer id,@RequestBody BlackList blackList);


    @RequestMapping(value = "/blacklists/passengers/{passenger_id}",method = RequestMethod.GET)
    public List<BlackList> getByPassenger(@PathVariable Integer passenger_id);

    @RequestMapping(value = "/blacklists/passenger/{passenger_id}/driver/{driver_id}",method = RequestMethod.DELETE)
    public void deleteBlackListByPerson(@PathVariable Integer passenger_id, @PathVariable Integer driver_id);
    // @RequestMapping(value = "/blacklists",method = RequestMethod.POST)
    @RequestMapping(value = "/blacklists/passenger/{passenger_id}/driver/{driver_id}", method = RequestMethod.GET)
    public BlackList findByPerson(@PathVariable Integer passenger_id, @PathVariable Integer driver_id);
}
