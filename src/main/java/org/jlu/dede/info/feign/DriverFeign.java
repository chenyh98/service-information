package org.jlu.dede.info.feign;
import org.jlu.dede.publicUtlis.model.*;
import org.jlu.dede.publicUtlis.model.Car;
import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface DriverFeign {
    @RequestMapping(value = "/drivers",method = RequestMethod.POST)
    //dataaccess中增加司机的接口
    public void addDriver(@RequestBody Driver driver);

    @RequestMapping(value = "/drivers/{id}",method = RequestMethod.POST)
    //dataaccess中编辑个人资料的接口
    public void updateDriver(@PathVariable Integer id,@RequestBody Driver driver);

    @RequestMapping(value = "/drivers/{id}",method = RequestMethod.GET)
    //dataaccess中查询个人资料的接口
    public Driver  getByID(@PathVariable Integer id);

    @PostMapping("/drivers/{id}/car")
    public void chooseCar(@PathVariable Integer id,@RequestBody Car car);

    @GetMapping("/drivers/{id}/state/{state}")
    @ResponseBody
    public void updateState(@PathVariable Integer id,@PathVariable String state);

    @GetMapping(value = "/drivers/account/{id}")
    public Driver findByAccount(@PathVariable  Integer id);

    @GetMapping("/drivers/blacklist/{id}")
    public Driver findByBlackList(@PathVariable Integer id);


}
