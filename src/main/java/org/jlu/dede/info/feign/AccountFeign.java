package org.jlu.dede.info.feign;
import org.jlu.dede.publicUtlis.model.*;
import org.jlu.dede.publicUtlis.model.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "data-access",url = "10.100.0.2:8765")
public interface AccountFeign {
    @PostMapping(value = "/accounts")
    public void addAccount(@RequestBody Account account);

    @PostMapping(value = "/accounts/{id}")
    public void updateAccount(@PathVariable Integer id,@RequestBody Account account);

    @GetMapping(value = "/accounts/{id}")
    public Account getByID(@PathVariable Integer id);

    @GetMapping(value = "/accounts/email/{emailname}")
    public Account getByEmail(@PathVariable String emailname);

    @GetMapping("/accounts/passenger/{id}")
    public Account getByPassenger(@PathVariable Integer id);

    @GetMapping("/accounts/driver/{id}")
    public Account getByDriver(@PathVariable Integer id);

}
