package org.jlu.dede.info.feign;
import org.jlu.dede.publicUtlis.model.*;
import org.jlu.dede.publicUtlis.model.Car;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface CarFeign {
    @RequestMapping(value = "/cars/{id}",method = RequestMethod.GET)
    //dataaccess中查询车的接口
    public Car getByID(@PathVariable Integer id);

    @RequestMapping(value = "/cars",method = RequestMethod.POST)
    //dataaccess中增加车的接口
    public void addCar(@RequestBody Car car);

    @RequestMapping(value = "/cars/{id}",method = RequestMethod.DELETE)
    //dataaccess中删除车的接口
    public void deleteCar(@PathVariable Integer id);

    @RequestMapping(value = "/cars/{id}",method = RequestMethod.POST)
    //dataaccess中改变车的接口
    public void updateCar(@PathVariable Integer id,@RequestBody Car car);

    @GetMapping("/cars/driver/{id}")
    public List<Car> getByDriver(@PathVariable Integer id);
    //@GetMapping(value = "")
    //dataaccess中改变司机工作状态的接口
}
