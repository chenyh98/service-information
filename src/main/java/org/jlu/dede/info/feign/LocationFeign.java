package org.jlu.dede.info.feign;
import org.jlu.dede.publicUtlis.model.Car;
import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "service-location",url="10.100.0.6:8770")
public interface LocationFeign {
    //传递司机id,两个数据库表统一
    @RequestMapping(value = "/location/getDriverId/{id}",method = RequestMethod.POST)
    public boolean getDriverId(@PathVariable Integer id);
}
