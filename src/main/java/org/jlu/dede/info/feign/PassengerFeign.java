package org.jlu.dede.info.feign;

//import org.springframework.http.ResponseEntity;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface PassengerFeign {
    @RequestMapping(value = "/passengers",method = RequestMethod.POST)
    //dataaccess中增加乘客的接口
    public void addPassenger(@RequestBody Passenger passenger);

    @RequestMapping(value = "/passengers/{id}",method = RequestMethod.POST)
    //dataaccess中编辑个人资料的接口
    public void updatePassenger(@PathVariable Integer id,@RequestBody Passenger passenger);

    @RequestMapping(value = "/passengers/{id}",method = RequestMethod.GET)
    //dataaccess中查询个人资料的接口
    public Passenger getByID(@PathVariable Integer id);

    @GetMapping(value = "/passengers/account/{id}")
    public Passenger findByAccount(@PathVariable  Integer id);

    @GetMapping("/passengers/blacklist/{id}")
    public Passenger findByBlackList(@PathVariable Integer id);


}

