package org.jlu.dede.info.util;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;
import com.sun.mail.util.MailSSLSocketFactory;
/**
 * @ClassName EmailUtil
 * @Description 用户邮箱的帮助类
 * @Author 过道
 * @Date 2018/12/22 21:21
 * @Version 1.0
 */

public class EmailUtil {
    // 发件人 账号和密码，我使用的是163邮箱，如何开启163邮箱的API接口，请百度。
    /**
     * 发送邮箱：XXX@163.com
     */
    private static final String MY_EMAIL_ACCOUNT = "449194323@qq.com";
    /**
     * 授权码：123456
     */
    private static final String MY_EMAIL_PASSWORD = "prmijeoqmxjvbjfh";// 密码,是你自己的设置的授权码

    // SMTP服务器(这里用的163 SMTP服务器)
    /**
     * 163代理服务器:smtp.163.com
     */
    private static final String MEAIL_QQ_SMTP_HOST = "smtp.qq.com";
    /**
     * 163 使用的端口:25
     */
    private static final String SMTP_QQ_PORT = "465";// 端口号,这个是163使用到的;QQ的应该是465或者875

    /**
     * @param targetAddr 目标邮箱地址
     * @param code       验证码
     */
    public static void sendMail(String targetAddr, String code) {
        Properties p = new Properties();
        p.setProperty("mail.smtp.host", MEAIL_QQ_SMTP_HOST);
        p.setProperty("mail.smtp.port", SMTP_QQ_PORT);
        p.setProperty("mail.smtp.socketFactory.port", SMTP_QQ_PORT);
        p.setProperty("mail.smtp.auth", "true");
        p.setProperty("mail.smtp.socketFactory.class", "SSL_FACTORY");
        p.setProperty("mail.smtp.starttls.enable", "true");
        //MailSSLSocketFactory sf = new MailSSLSocketFactory();
        //sf.setTrustAllHosts(true);
        p.setProperty("mail.smtp.ssl.enable", "true");
        //p.setProperty("mail.smtp.ssl.socketFactory", sf);
        //”mail.smtp.starttls.enable”, true

        Session session = Session.getInstance(p, new Authenticator() {
            // 设置认证账户信息
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(MY_EMAIL_ACCOUNT, MY_EMAIL_PASSWORD);
            }
        });
//        session.setDebug(true);
        MimeMessage message = new MimeMessage(session);
        try {
            // 发件人
            message.setFrom(new InternetAddress(MY_EMAIL_ACCOUNT));
            // 收件人和抄送人
            message.setRecipients(Message.RecipientType.TO, targetAddr);

            // 内容
            message.setSubject("的的打车");
            message.setContent("您的验证码为 : <h1>" + code+"</h1>", "text/html;charset=UTF-8");
            message.setSentDate(new Date());
            message.saveChanges();
            Transport.send(message);
            //return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            //return false;
        }

    }

}
