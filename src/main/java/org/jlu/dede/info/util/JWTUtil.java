package org.jlu.dede.info.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.jlu.dede.info.json.*;


public class JWTUtil {
    public static final  String SECRET="JSKKhd&^bdjh3C5#%%gt!";//token秘钥，请勿随意修改或泄露
    //public static  final  int CALENDAR_FIELD= Calendar.DATE;//获取年月日日期中得 日
    public static  final  int CALENDAR_INTERVAL=14400000;//token过期时间为4h
    public static  final int refreshBefore=3600000;//token刷新时间1h
    /*
    JWT生成token方法，登录成功后返回token
     */
    public static String createToken(Integer user_id,Integer account_id){
        Date now=new Date();//签发时间
        long expiremill=now.getTime()+CALENDAR_INTERVAL;
        Date expireDate =new Date(expiremill);//获取过期时间

        //头部
        Map<String,Object> map=new HashMap<>();
        map.put("alg","HS256");
        map.put("typ","JWT");


        /*token创建*/
        String  token=JWT.create().withHeader(map)
                .withClaim("iss","KeYanZhiXing")
                .withClaim("user_id",String.valueOf(user_id))
                .withClaim("account_id",String.valueOf(account_id))
                .withIssuedAt(now)
                .withExpiresAt(expireDate)
                .sign(Algorithm.HMAC256(SECRET));
        return  token;
    }
    /*
      解密token
    */
    public  static  DecodedJWT verifyToken(String token){
        DecodedJWT jwt=null;
        try{
        JWTVerifier verifier=JWT.require(Algorithm.HMAC256(SECRET)).withIssuer("KeYanZhiXing").build();

        jwt=verifier.verify(token);
        }catch (JWTVerificationException e){
            throw new AuthFailException("Unavailable token");
         }
        return  jwt;
    }




}

