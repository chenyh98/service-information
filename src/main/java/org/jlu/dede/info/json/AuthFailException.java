package org.jlu.dede.info.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class AuthFailException extends RestException {
    public AuthFailException(String msg) {
        super(704, msg);
    }
}
