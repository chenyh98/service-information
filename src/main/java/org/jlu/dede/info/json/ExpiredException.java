package org.jlu.dede.info.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class ExpiredException extends RestException {
    public ExpiredException(String msg) {
        super(703, msg+" has expired");
    }
}
