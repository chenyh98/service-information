package org.jlu.dede.info.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class PasswdWrong extends RestException {

     public PasswdWrong(String msg) {
         super(706, msg + " is wrong");
     }


}
