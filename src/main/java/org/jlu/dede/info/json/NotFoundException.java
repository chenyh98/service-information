package org.jlu.dede.info.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class NotFoundException extends RestException {
    public NotFoundException(String msg) {
        super(705, msg+" is not found");
    }
}
