package org.jlu.dede.info.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class AlreadyExitException extends RestException {
    public AlreadyExitException(String msg) { super(701, msg+" has exited");
    }
}
