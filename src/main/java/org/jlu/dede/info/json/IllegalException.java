package org.jlu.dede.info.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class IllegalException extends RestException {
    public IllegalException(String msg) {
        super(702,msg+" is illegal");
    }
}
