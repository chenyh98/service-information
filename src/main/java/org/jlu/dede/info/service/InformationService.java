package org.jlu.dede.info.service;
import org.jlu.dede.info.feign.*;
import org.jlu.dede.info.json.*;
import org.jlu.dede.info.util.EmailUtil;
import org.jlu.dede.info.util.FormatUtil;
import org.jlu.dede.info.util.JWTUtil;
import org.jlu.dede.publicUtlis.model.*;
//import org.jlu.dede.feign.*;
//2.注册保存时要传入乘客或司机的email
import org.jlu.dede.publicUtlis.json.AuthFailException;
import org.jlu.dede.publicUtlis.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.StringRedisTemplate;

import com.auth0.jwt.interfaces.DecodedJWT;

@Service
//@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InformationService {
    @Autowired
    private PassengerFeign passengerFeign;
    @Autowired
    private BlacklistFeign blacklistFeign;
    @Autowired
    private AccountFeign accountFeign;
    @Autowired
    private CarFeign carFeign;
    @Autowired
    private StringRedisTemplate srt;

    //----------------------------------注册

    //验证乘客邮箱是否有效且唯一
    public boolean checkEmailPassenger(String emailAccount) {//是否传入乘客type？这样才能从type为乘客的账户中寻找是否已经存在
        boolean inf1,inf2;
        //验证邮箱是否已经存在于乘客数据库中
        Account account=accountFeign.getByEmail(emailAccount);
        //是否要加一个抛出已经存在于数据库的异常？
        if(account==null)
            inf1=true;
        else {
            inf1 = false;
            throw new AlreadyExitException(emailAccount);
        }
        //校验邮箱是否合法
        inf2=FormatUtil.isEmail(emailAccount);
        if(inf2==false){
            throw new IllegalException(emailAccount);
        }

        if(inf1==true&&inf2==true)
            return true;
        else
            return false;
    }
    //验证司机邮箱是否有效且唯一
    public boolean checkEmailDriver(String emailAccount) {//是否传入司机type？这样才能从type为乘客的账户中寻找是否已经存在
        boolean inf1,inf2;
        //验证邮箱是否已经存在于司机数据库中
        Account account=accountFeign.getByEmail(emailAccount);
        //是否要加一个抛出已经存在于数据库的异常？
        if(account==null)
            inf1=true;
        else {
            inf1 = false;
            throw new AlreadyExitException(emailAccount);
        }
        //校验邮箱是否合法
        inf2=FormatUtil.isEmail(emailAccount);
        if(inf2==false){
            throw new IllegalException(emailAccount);
        }

        if(inf1==true&&inf2==true)
            return true;
        else
            return false;
    }



    //@Autowired
    //private RedisTemplate<String, String> redisTemplate;
    //生成验证码保存在redis并发送验证码
    public boolean createVerifyCode(String emailAccount) {
        //生成随机验证码
        Random random = new Random();
        StringBuilder code = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            code.append(random.nextInt(10));
        }
        String message = code.toString();
        //保存在redis中，3分钟过期
        srt.opsForValue().set(message, emailAccount, 180, TimeUnit.SECONDS);//感觉可加异常
        //发送验证码
        EmailUtil.sendMail(emailAccount,message);//感觉可加异常
        return true;
    }



    //验证验证码
    public boolean checkVerifyCode(String verifyCode,String emailAccount) {
        //如果验证码对应的email与我们的相同证明正确
        String emailAccountInRedis =  srt.opsForValue().get(verifyCode);
        if(emailAccountInRedis!=null) {
            if (emailAccount.equals(emailAccountInRedis)) {
                return true;
            }
            else
                throw new NotFoundException(emailAccount);
        }
        else
            throw new NotFoundException(emailAccount);

    }

    //保存密码并创建一个乘客实体存入数据库

    public boolean storePasswordPassenger(String password,String emailAccount) {
        Passenger passenger = new Passenger();
        passenger.setX(null);
        passenger.setY(null);
        passenger.setCertificate(null);
        Account account=new Account();
        account.setName(null);
        account.setAge(null);
        account.setSex(null);
        account.setPassword(password);
        account.setPhone(null);
        account.setEmail(emailAccount);
        //截取当前系统时间
        Date currentTime = new Date();
        //改变输出格式（自己想要的格式）
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //得到字符串时间
        String s8 = formatter.format(currentTime);
        account.setRegisterTime(s8);
        account.setVerifyValue(100);
        account.setMoney(BigDecimal.valueOf(0));
        account.setType(1);
        account.setState(0);
        passenger.setAccount(account);
        passenger.setOrder(null);
        passenger.setCompanyAddress(null);
        passenger.setCommonAddressOne(null);
        passenger.setCommonAddressTwo(null);
        //accountFeign.addAccount(account);
        passengerFeign.addPassenger(passenger);
        return true;
    }

    //保存密码并创建一个司机实体存入数据库
    @Autowired
    DriverFeign driverFeign;
    @Autowired
    LocationFeign locationFeign;
    public boolean storePasswordDriver(String password,String emailAccount) {
        Driver driver = new Driver();
        driver.setX(null);
        driver.setY(null);
        driver.setCertificate(null);
        driver.setCurrentCar(null);
        driver.setState(0);
        Account account=new Account();
        account.setName(null);
        account.setAge(null);
        account.setSex(null);
        account.setPassword(password);
        account.setPhone(null);
        account.setEmail(emailAccount);
        //截取当前系统时间
        Date currentTime = new Date();
        //改变输出格式（自己想要的格式）
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //得到字符串时间
        String s8 = formatter.format(currentTime);
        account.setRegisterTime(s8);
        account.setVerifyValue(100);
        account.setMoney(BigDecimal.valueOf(0));
        account.setType(2);
        account.setState(0);
        driver.setAccount(account);
        driver.setOrder(null);
        driverFeign.addDriver(driver);
        locationFeign.getDriverId(0);
        return true;
    }




    //-----------------------------------登录和登出



    public void logout(String token) {
        if (token == null) {
             throw new AuthFailException("Unauthorized user");
            //   异常处或返回错误信息
        }
        DecodedJWT jwt = JWTUtil.verifyToken(token);
        String user_id = jwt.getClaim("user_id").asString();
        String account_id = jwt.getClaim("account_id").asString();
        String sign = jwt.getSignature();
        String abandon = srt.opsForValue().get(sign);

        if (abandon != null && abandon.equals(account_id)) {//说明令牌已经作废
             throw new ExpiredException(user_id);
            //异常处理或返回错误信息
        }

        Date now = new Date();//验证退出的令牌为超时
        Date expire = jwt.getExpiresAt();
        long diff = expire.getTime() - now.getTime();

        if (diff <= 0) {
             throw new ExpiredException(user_id);
            //异常处理或返回错误信息
        }
        Account account=accountFeign.getByID(Integer.parseInt(account_id));
        account.setState(0);
        accountFeign.updateAccount(Integer.parseInt(account_id),account);
        // add token to abandon list
        srt.opsForValue().set(sign, account_id, diff, TimeUnit.MILLISECONDS);
    }
    public String checkPasswordPassenger(String emailAccount, String password){
        System.out.println("begin teste!!!!");
        String token=null;
        Integer account_id =accountFeign.getByEmail(emailAccount).getId();//"调用数据接入层接口";
        System.out.println("getaccount_id"+account_id);
        Integer user_id =passengerFeign.findByAccount(account_id).getId();
        System.out.println("getuser_id"+user_id);
        String pw=accountFeign.getByEmail(emailAccount).getPassword();
        System.out.println("getpw"+pw);
        System.out.println(password);
        if(pw.equals(password)) {
            System.out.println("password TRUE!!!");
            token = JWTUtil.createToken(user_id, account_id);
            Account account=accountFeign.getByPassenger(user_id);
            account.setState(1);
            accountFeign.updateAccount(account_id,account);
        }
        else{
            token = null;
            throw new PasswdWrong(password);
        }

        return token;
//        String token=null;
//        Integer account_id =accountFeign.getByEmail(emailAccount).getId();//"调用数据接入层接口";
//        Integer user_id =passengerFeign.findByAccount(account_id).getId();
//        String pw=accountFeign.getByEmail(emailAccount).getPassword();
//        if(pw.equals(password)) {
//            token = JWTUtil.createToken(user_id, account_id);
//            Account account=accountFeign.getByPassenger(user_id);
//            account.setState(1);
//            accountFeign.updateAccount(account_id,account);
//        }
//        else
//            token = null;
//
//        return token;
    }
    public String checkPasswordDriver(String emailAccount, String password){//该方法既是乘客端的也是司机端的
        String token=null;
        Integer account_id=accountFeign.getByEmail(emailAccount).getId();//"调用数据接入层接口";
        accountFeign.getByEmail(emailAccount).setState(1);
        Integer user_id=driverFeign.findByAccount(account_id).getId();//"调用数据接入层接口";
        String pw=accountFeign.getByEmail(emailAccount).getPassword();
        if(pw.equals(password)) {
            System.out.println("password TRUE!!!");
            token = JWTUtil.createToken(user_id, account_id);
            Account account=accountFeign.getByEmail(emailAccount);
            account.setState(1);
            accountFeign.updateAccount(account_id,account);
        }
        else {
            token = null;
            throw new PasswdWrong(password);
        }

        return token;
    }
    public Map<String, Object> verification(String token) {
        //验证，进行验证，设置刷新时间，到刷新时间自动刷新，减去强制用户登录的麻烦
        if (token == null) {
            //System.out.println("错误");
            throw new AuthFailException("Unauthorized user");

            //用户为空说明是未查询到的ID
        }
        DecodedJWT jwt = JWTUtil.verifyToken(token);
        String account_id=jwt.getClaim("account_id").asString();
        String user_id = jwt.getClaim("user_id").asString();
//
        // check if token has been abandoned
        String sign = jwt.getSignature();
        String abandon = srt.opsForValue().get(sign);
        if (abandon != null && abandon.equals(account_id)) {//说明令牌已经作废
            //抛出异常或返回错误信息
            throw new ExpiredException(user_id);
            //异常处理或返回错误信息
        }

        Date expire = jwt.getExpiresAt();
        Date now = new Date();
//        // already expire, auth fail
        if (expire.before(now)) {//令牌已经过期
            throw new ExpiredException(user_id);
            //异常处理或返回错误信息
        }
        Map<String,Object> result=new HashMap<>();
        result.put("user_id",user_id);
        result.put("account_id",account_id);

        // about to expire, renew token
        if (expire.getTime() - now.getTime() < JWTUtil.refreshBefore) {
            token = JWTUtil.createToken(Integer.parseInt(user_id),Integer.parseInt(account_id));
            result.put("token", token);
        }
//
        return result;
//
    }


    //-------------------------------------个人资料


    //更新乘客个人资料
    public boolean storePassenger(Integer id,Passenger passenger){
        passengerFeign.updatePassenger(id,passenger);
        return true;
    }


    //更新司机个人资料
    public boolean storeDriver(Integer id,Driver driver){
        driverFeign.updateDriver(id,driver);
        return true;
    }


    //返回乘客个人资料
    public Passenger returnPassenger(int id){
        return passengerFeign.getByID(id);
    }


    //返回司机个人资料
    public Driver returnDriver(Integer id){
        return driverFeign.getByID(id);
    }//getByID重复

    //返回乘客账户
    public Account queryPassengerAccount(Integer id){
        return accountFeign.getByPassenger(id);
    }

    //返回司机账户
    public Account queryDriverAccount(Integer id){
        return accountFeign.getByDriver(id);
    }

    //--------------------------------------????

    //保存黑名单
    public boolean storeBlackList(Integer id, Driver driver){
        BlackList blackList=new BlackList();
        Passenger passenger=passengerFeign.getByID(id);
        blackList.setPassenger(passenger);
        blackList.setDriver(driver);
        List<BlackList> list=blacklistFeign.getByPassenger(id);
        if(list!=null) {
            for (BlackList bl : list) {
                Passenger passenger1=passengerFeign.findByBlackList(bl.getId());
                Driver driver1=driverFeign.findByBlackList(bl.getId());
                if (passenger1.getId() == id && driver1.getId() == driver.getId()) {
                    throw new AlreadyExitException("this blacklist");
                }

            }
        }
        blacklistFeign.addBlackList(blackList);
        return true;
    }


//------------------------------------------

    //通过乘客返回黑名单
    public List returnBlackList(Integer id){

        return blacklistFeign.getByPassenger(id);
    }


    //通过id删除黑名单
    public boolean deleteBlackList(Integer id){
        BlackList blackList=blacklistFeign.getByID(id);
        if(blackList!=null) {
            blacklistFeign.deleteBlackList(id);
            return true;
        }
        throw new NotFoundException("this blacklist");
    }//没有提供根据乘客和司机查找黑名单的接口

    //通过乘客和司机删除黑名单
    public boolean deleteBlackListByPerson(Integer id1,Integer id2){
        BlackList blackList=blacklistFeign.findByPerson(id1,id2);
        if(blackList!=null) {
            blacklistFeign.deleteBlackListByPerson(id1, id2);
            return true;
        }
        throw new NotFoundException("this blacklist!");
    }//没有提供根据乘客和司机查找黑名单的接口


    //    //保存车
    public boolean storeCar(Integer id, Car car) {
        Driver driver = driverFeign.getByID(id);
        // 可以加一个异常
        List<Car> list = carFeign.getByDriver(id);
        if (list != null){
            for (Car car1 : list) {
                if (car1.getNum().equals(car.getNum()))
                    throw new AlreadyExitException("this car");
            }
        }
        car.setDriver(driver);
        carFeign.addCar(car);
        return true;

    }




    //    //返回车
    public List returnCar(Integer id){
        return carFeign.getByDriver(id);
    }



    //删除车
    public boolean deleteCar(Integer id){
        Car car=carFeign.getByID(id);
        if(car!=null) {
            carFeign.deleteCar(id);
            return true;
        }
        throw new NotFoundException("this car!");
    }
    //选择车
    public boolean chooseCar(Integer id,Car car){
        driverFeign.chooseCar(id,car);
        return true;
    }
    //改变车
    //public boolean changeCar(Car car){

    //}

//    //修改司机状态
    public boolean startWork(Integer id, String state){
        driverFeign.updateState(id,state);
        return  true;
    }

}

